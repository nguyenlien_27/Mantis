package pageObject;

import org.openqa.selenium.By;

public class logInObject {
	//user name text box
	public static By txtUsername = By.name("username");
			
	//pass word
	public static By txtPassword = By.name("password");
			
	//log in button
	public static By btnSubmit = By.xpath("html/body/div[3]/form/table/tbody/tr[6]/td/input");
}
