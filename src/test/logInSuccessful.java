package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

import common.utils;
import pageObject.logInObject;

public class logInSuccessful {
	@Before
	public void before(){
		System.setProperty("webdriver.gecko.driver",utils.Geckodriver);
		utils.driver = new FirefoxDriver();
	}
	@Test
	public void check() throws InterruptedException {
		utils.driver.get(utils.URL);
		utils.driver.findElement(logInObject.txtUsername).sendKeys(utils.Username);
		utils.driver.findElement(logInObject.txtPassword).sendKeys(utils.Password);
		utils.driver.findElement(logInObject.btnSubmit).click();
		Thread.sleep(8000);
	}
	
	@After
	public void after() throws Exception {
		utils.driver.quit();
	}
}
